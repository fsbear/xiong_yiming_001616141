/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donation;


import Business.Role.NurseRole;
import Business.WorkQueue.LabTestWorkRequest;
import java.util.Date;

/**
 *
 * @author xiongyiming
 */
public class DonationWorkRequest extends LabTestWorkRequest{
    private String bloodtype; 
    private Date date;
    private String barCode;
    private NurseRole nurse;
    
    public DonationWorkRequest(){
        super();
        date = new Date();
              
        
    }

    public String getBloodtype() {
        return bloodtype;
    }

    public void setBloodtype(String bloodtype) {
        this.bloodtype = bloodtype;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public NurseRole getNurse() {
        return nurse;
    }

    public void setNurse(NurseRole nurse) {
        this.nurse = nurse;
    }

    public Date getDate() {
        return date;
    }
    
    
}
