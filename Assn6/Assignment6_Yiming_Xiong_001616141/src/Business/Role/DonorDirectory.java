/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;


import Business.Employee.EmployeeDirectory;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class DonorDirectory extends EmployeeDirectory{
    private ArrayList<DonorRole> donorList;
    
    public DonorDirectory() {
        donorList = new ArrayList<>();
    }

    public ArrayList<DonorRole> getDonorList() {
        return donorList;
    }

    
    
    public DonorRole createDonor(String name, String gender, int age){
        DonorRole d = new DonorRole();
        d.setName(name);
        d.setAge(age);
        d.setGender(gender);
        donorList.add(d);
        return d;
    }
}
