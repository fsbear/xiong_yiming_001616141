/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Employee.Employee;
import Business.Donation.DonationWorkRequest;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author xiongyiming
 */
public class DonorRole extends Role{
    private String name;
    private String gender;
    private int age;
    private ArrayList<DonationWorkRequest> donationHistory;

    public DonorRole(){
        donationHistory = new ArrayList<DonationWorkRequest>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ArrayList<DonationWorkRequest> getDonationHistory() {
        return donationHistory;
    }

    public void setDonationHistory(ArrayList<DonationWorkRequest> donationHistory) {
        this.donationHistory = donationHistory;
    }

    @Override
    public JPanel createWorkArea(JPanel pnl, UserAccount ua, Organization o, Enterprise e, EcoSystem es) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    
}
