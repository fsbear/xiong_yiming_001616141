/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.GovernmentOrganization.GovernmentOrganizationDirectory;
import Business.GuardianOrganization.GuardianOrganizationDirectory;
import Business.HospitalOrganization.Organization;
import Business.HospitalOrganization.HospitalOrganizationDirectory;
import Business.HouseManageOrganization.HouseManageOrganizationDirectory;
import Business.Role.ElderlyDirectory;
import Business.VolunteerOrganization.VolunteerOrganizationDirectory;

/**
 *
 * @author raunak
 */
public abstract class Enterprise extends Organization{

    private EnterpriseType enterpriseType;
    private HospitalOrganizationDirectory hospitalOrganizationDirectory;
    private GovernmentOrganizationDirectory governmentOrganizationDirectory;
    private VolunteerOrganizationDirectory volunteerOrganizationDirectory;
    private HouseManageOrganizationDirectory houseManageOrganizationDirectory;
    private GuardianOrganizationDirectory guardianOrganizationDirectory;
    private ElderlyDirectory elderlyDirectory;
    
    public Enterprise(String name, EnterpriseType type) {
        super(name);
        this.enterpriseType = type;
        this.hospitalOrganizationDirectory = new HospitalOrganizationDirectory();
        this.governmentOrganizationDirectory = new GovernmentOrganizationDirectory();
        this.guardianOrganizationDirectory = new GuardianOrganizationDirectory();
        this.houseManageOrganizationDirectory = new HouseManageOrganizationDirectory();
        this.volunteerOrganizationDirectory = new VolunteerOrganizationDirectory();
        this.elderlyDirectory = new ElderlyDirectory();
    }
    
    public enum EnterpriseType{
        Hospital("Hospital"),
        Government("Government"),
        Volunteer("Volunteer"),
        House("House"),
        Guardian("Guardian");
        
        
        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public HospitalOrganizationDirectory getHospitalOrganizationDirectory() {
        return hospitalOrganizationDirectory;
    }

    public GovernmentOrganizationDirectory getGovernmentOrganizationDirectory() {
        return governmentOrganizationDirectory;
    }

    public VolunteerOrganizationDirectory getVolunteerOrganizationDirectory() {
        return volunteerOrganizationDirectory;
    }

    public HouseManageOrganizationDirectory getHouseManageOrganizationDirectory() {
        return houseManageOrganizationDirectory;
    }

    public GuardianOrganizationDirectory getGuardianOrganizationDirectory() {
        return guardianOrganizationDirectory;
    }

    public ElderlyDirectory getElderlyDirectory() {
        return elderlyDirectory;
    }

    

}
