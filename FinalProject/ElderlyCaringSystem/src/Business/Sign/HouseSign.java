/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Sign;

import java.util.Date;

/**
 *
 * @author xiongyiming
 */
public class HouseSign {
    private double temperature;
    private double humidity;
    private double gas;
    private double smoke;
    private Date date;

    public HouseSign(){
        date = new Date();
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getGas() {
        return gas;
    }

    public void setGas(double gas) {
        this.gas = gas;
    }

    public double getSmoke() {
        return smoke;
    }

    public void setSmoke(double smoke) {
        this.smoke = smoke;
    }

    public Date getDate() {
        return date;
    }
    
    
    
    
}
