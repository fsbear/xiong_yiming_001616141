/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Sign;

import java.util.Date;

/**
 *
 * @author xiongyiming
 */
public class VitalSign {
    private double respiratory_rate;
    private double heart_rate;
    private double bloodpressure;
    private double weight;
    private Date date;
    private String status;

    
    public VitalSign(){
        date = new Date();
    }

    public double getRespiratory_rate() {
        return respiratory_rate;
    }

    public void setRespiratory_rate(double respiratory_rate) {
        this.respiratory_rate = respiratory_rate;
    }

    public double getHeart_rate() {
        return heart_rate;
    }

    public void setHeart_rate(double heart_rate) {
        this.heart_rate = heart_rate;
    }

    public double getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(double bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Date getDate() {
        return date;
    }
    public void  check(){
        if(respiratory_rate>11 && respiratory_rate<21 && heart_rate>54 && heart_rate<106
                   && weight>110 && bloodpressure>109 && bloodpressure<121)
               status =  "Normal";
           else
               status =  "Abnormal";
    }

    public String getStatus() {
        return status;
    }
    public void simulate(){
        this.respiratory_rate = 15* Math.random()+5;
        this.heart_rate = 50* Math.random()+50;
        this.weight = 100* Math.random()+100;
        this.bloodpressure = 16* Math.random()+105;
        this.date = new Date();
    }
    
    
    
}
