/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.House;

import Business.Sign.HouseSign;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class HouseDirectory {
    private ArrayList<House> houseList;
    
    public HouseDirectory() {
        houseList = new ArrayList<>();
    }
    
     public House addHouse(House h){
        
        houseList.add(h);
        return h;
        
        
    }
    public void deleHouse(House vs){
        houseList.remove(vs);
        
    }

    public ArrayList<House> getHouseList() {
        return houseList;
    }
    
}
