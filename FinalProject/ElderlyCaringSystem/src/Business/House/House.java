/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.House;

import Business.Role.Elderly;
import Business.Sensors.GasSensor;
import Business.Sensors.HumiditySensor;
import Business.Sensors.SmokeSensor;
import Business.Sensors.TemperatureSensor;
import Business.Sign.HouseSign;
import Business.Sign.HouseSignHistory;

/**
 *
 * @author xiongyiming
 */
public class House {
    private Elderly elderly;
    private String address;
    private String phone;
    private GasSensor gasSensor;
    private SmokeSensor smokeSensor;
    private HumiditySensor hunmiditySensor;
    private TemperatureSensor temperatureSensor;
    private HouseSign houseSign;
    private HouseSignHistory houseSignHistory;
    private boolean isNormal = false;
    
    
    public House(){
//        this.elderly = elderly;
//        this.address = elderly.getAddress();
//        this.phone = elderly.getMobile();
        this.gasSensor = new GasSensor();
        this.smokeSensor = new SmokeSensor();
        this.hunmiditySensor = new HumiditySensor();
        this.temperatureSensor = new TemperatureSensor();
        
        this.houseSignHistory = new HouseSignHistory();
    }
    
    public void registerResident(Elderly elderly){
        this.elderly = elderly;
        //this.address = elderly.getAddress();
        this.phone = elderly.getMobile();
    }
    
    
    public void checkHouse(){
        this.houseSign = new HouseSign();
        gasSensor.generate();
        hunmiditySensor.generate();
        smokeSensor.generate();    
        temperatureSensor.generate();
        
        
        houseSign.setGas(gasSensor.getGasConcentration());
        houseSign.setHumidity(hunmiditySensor.getHumidity());
        houseSign.setSmoke(smokeSensor.getSmokeConcentration());
        houseSign.setTemperature(temperatureSensor.getTemperature());
        houseSign.getDate();
        houseSignHistory.addHouseSign(houseSign);
        
        if(houseSign.getGas()<=5.00&&houseSign.getHumidity()<=5.00&&houseSign.getSmoke()<=5.00&&houseSign.getTemperature()<=5.00){
            isNormal = true;
        }
        else
            isNormal = false;
        
    }
    public void safeCheckHouse(){
        this.houseSign = new HouseSign();
        gasSensor.safegenerate();
        hunmiditySensor.safegenerate();
        smokeSensor.safegenerate();    
        temperatureSensor.safegenerate();
        
        
        houseSign.setGas(gasSensor.getGasConcentration());
        houseSign.setHumidity(hunmiditySensor.getHumidity());
        houseSign.setSmoke(smokeSensor.getSmokeConcentration());
        houseSign.setTemperature(temperatureSensor.getTemperature());
        houseSign.getDate();
        houseSignHistory.addHouseSign(houseSign);
        
        if(houseSign.getGas()<=5.00&&houseSign.getHumidity()<=5.00&&houseSign.getSmoke()<=5.00&&houseSign.getTemperature()<=5.00){
            isNormal = true;
        }
        else
            isNormal = false;
        
    }

    public Elderly getElderly() {
        return elderly;
    }

    public void setElderly(Elderly elderly) {
        this.elderly = elderly;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isIsNormal() {
        return isNormal;
    }

    public HouseSign getHouseSign() {
        return houseSign;
    }

    public HouseSignHistory getHouseSignHistory() {
        return houseSignHistory;
    }

    @Override
    public String toString() {
        return address ;
    }
    
   
    
    
}
