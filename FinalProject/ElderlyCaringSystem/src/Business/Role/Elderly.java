/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.House.House;
import Business.Role2.GuardianRole;
import Business.Sign.VitalSign;
import Business.Sign.VitalSignHistory;

/**
 *
 * @author xiongyiming
 */
public class Elderly extends Person{
    private GuardianRole guardian;
    private String appearenceDescription;
    private House house;
    private VitalSignHistory vsHistory;
    private VitalSign vs;
    private boolean volunteerButton;
    
    public Elderly(){
        this.vsHistory = new VitalSignHistory();
        this.vs = new VitalSign();
    }

    public GuardianRole getGuardian() {
        return guardian;
    }

    public void setGuardian(GuardianRole guardian) {
        this.guardian = guardian;
    }

    public String getAppearenceDescription() {
        return appearenceDescription;
    }

    public void setAppearenceDescription(String appearenceDescription) {
        this.appearenceDescription = appearenceDescription;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public VitalSignHistory getVsHistory() {
        return vsHistory;
    }

    public void setVsHistory(VitalSignHistory vsHistory) {
        this.vsHistory = vsHistory;
    }

    public VitalSign getVs() {
        return vs;
    }

    public void setVs(VitalSign vs) {
        this.vs = vs;
    }

    public boolean isVolunteerButton() {
        return volunteerButton;
    }

    public void setVolunteerButton(boolean volunteerButton) {
        this.volunteerButton = volunteerButton;
    }
    
    
    
}
