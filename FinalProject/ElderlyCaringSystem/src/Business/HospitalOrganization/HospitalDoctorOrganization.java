/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HospitalOrganization;

import Business.Role2.HospitalDoctorRole;
import Business.Role2.Role;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class HospitalDoctorOrganization extends Organization{

    public HospitalDoctorOrganization() {
        super(Organization.Type.HospitalDoctor.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new HospitalDoctorRole());
        return roles;
    }
     
}