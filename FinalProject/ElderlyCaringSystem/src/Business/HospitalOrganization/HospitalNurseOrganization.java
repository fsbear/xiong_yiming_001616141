/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HospitalOrganization;

import Business.Role2.HospitalNurseRole;
import Business.Role2.Role;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class HospitalNurseOrganization extends Organization{

    public HospitalNurseOrganization() {
        super(Organization.Type.HospitalNurse.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new HospitalNurseRole());
        return roles;
    }
     
   
    
    
}
