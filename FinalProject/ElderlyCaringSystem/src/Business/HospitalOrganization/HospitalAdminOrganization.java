/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HospitalOrganization;

import Business.Role2.HospitalAdminRole;
import Business.Role2.Role;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class HospitalAdminOrganization extends Organization{

    public HospitalAdminOrganization() {
        super(Type.HospitalAdmin.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new HospitalAdminRole());
        return roles;
    }
     
}
