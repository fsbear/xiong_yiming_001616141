/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HospitalOrganization;

import Business.HospitalOrganization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class HospitalOrganizationDirectory {
    
    private ArrayList<Organization> hospitalOrganizationList;

    public HospitalOrganizationDirectory() {
        hospitalOrganizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return hospitalOrganizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.HospitalDoctor.getValue())){
            organization = new HospitalDoctorOrganization();
            hospitalOrganizationList.add(organization);
        }
        else if (type.getValue().equals(Type.HospitalNurse.getValue())){
            organization = new HospitalNurseOrganization();
            hospitalOrganizationList.add(organization);
        }
        return organization;
    }
}