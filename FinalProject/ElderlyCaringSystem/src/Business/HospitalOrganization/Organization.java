/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HospitalOrganization;

import Business.Employee.EmployeeDirectory;
import Business.Role2.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter;
    
    public enum Type{
        HospitalAdmin("HospitalAdmin Organization"), 
        HospitalDoctor("HospitalDoctor Organization"), 
        HospitalNurse("HospitalNurse Organization"),
        VolunteerAdmin("VolunteerAdmin Organization"),
        VolunteerCollege("VolunteerCollege Organization"),
        VolunteerCommunity("VolunteerCommunity Organization"),
        HouseManagerAdmin("HouseManagerAdmin Organization"),
        HouseManagerWorker("HouseManagerWorker Organization"),
        GuardianAdmin("GuardianAdmin Organization"),
        Guardian("Guardian Organization"),
        
        GovernmentManage("GovernmentManage Organization"),
        GovernmentAdmin("GovernmentAdmin Organization");
        
        
        
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
