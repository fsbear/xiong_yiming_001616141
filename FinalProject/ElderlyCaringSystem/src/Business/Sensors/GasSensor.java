/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Sensors;

import Business.Simulate.SimulationThread;

/**
 *
 * @author xiongyiming
 */
public class GasSensor extends Sensor{
    private double gasConcentration;
    private double upperlimit = 10.00;
    private double lowerlimit = 0.00;
    private double safeupperlimit = 5.00;
    private double safelowerlimit = 0.00;
    
    

    public GasSensor(){
        super();
        
        
        
    }
    public void safegenerate(){
        SimulationThread st = new SimulationThread(this, safeupperlimit, safelowerlimit);
        Thread t = new Thread(st);
        t.start();
        this.gasConcentration = st.getSimulateresult();
        t.interrupt();
    } 

    

    
    
    public void generate(){
        SimulationThread st = new SimulationThread(this, upperlimit, lowerlimit);
        Thread t = new Thread(st);
        t.start();
        this.gasConcentration = st.getSimulateresult();
        t.interrupt();
    } 

    public double getGasConcentration() {
        return gasConcentration;
    }
    
    
    
    
}
