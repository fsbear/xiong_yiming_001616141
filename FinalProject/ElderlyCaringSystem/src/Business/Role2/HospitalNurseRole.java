/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role2;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.HospitalOrganization.HospitalNurseOrganization;
import Business.HospitalOrganization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.HospitalNurseRole.HospitalNurseWorkAreaJPanel;

/**
 *
 * @author raunak
 */
public class HospitalNurseRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new HospitalNurseWorkAreaJPanel(userProcessContainer, account, (HospitalNurseOrganization)organization, enterprise, business);
    }
    
}
