/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role2;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.HospitalOrganization.Organization;
import Business.Role.Elderly;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.GuardianRole.GuardianWorkAreaJPanel;

/**
 *
 * @author xiongyiming
 */

public class GuardianRole extends Role{
private Elderly elderly;

    public Elderly getElderly() {
        return elderly;
    }

    public void setElderly(Elderly elderly) {
        this.elderly = elderly;
    }



    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new GuardianWorkAreaJPanel(userProcessContainer, enterprise, account);
    }
    public String toString(){
        return this.getFname()+this.getLname();
    }
}
