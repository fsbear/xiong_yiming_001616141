/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role2;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.HospitalOrganization.HospitalDoctorOrganization;
import Business.HospitalOrganization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.LabAssistantRole.LabAssistantWorkAreaJPanel;
import javax.swing.JPanel;


/**
 *
 * @author raunak
 */
public class HospitalDoctorRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new LabAssistantWorkAreaJPanel(userProcessContainer, account, (HospitalDoctorOrganization)organization, business);
    }
    
    
}
