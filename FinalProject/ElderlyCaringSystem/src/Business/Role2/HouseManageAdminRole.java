/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role2;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.HospitalOrganization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.HouseManageAdminRole.HouseManageAdminWorkAreaJPanel;

/**
 *
 * @author xiongyiming
 */
public class HouseManageAdminRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new HouseManageAdminWorkAreaJPanel(userProcessContainer, enterprise);
    }
}
