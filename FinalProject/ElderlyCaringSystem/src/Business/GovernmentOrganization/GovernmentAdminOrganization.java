/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GovernmentOrganization;

import Business.HospitalOrganization.Organization;
import Business.Role2.GovernmentAdminRole;
import Business.Role2.Role;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class GovernmentAdminOrganization extends Organization{
     public GovernmentAdminOrganization() {
        super(Organization.Type.GovernmentAdmin.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new GovernmentAdminRole());
        return roles;
    }
}
