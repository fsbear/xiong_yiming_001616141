/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GovernmentOrganization;

import Business.HospitalOrganization.Organization;
import Business.VolunteerOrganization.VolunteerCollegeOrganization;
import Business.VolunteerOrganization.VolunteerCommunityOrganization;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class GovernmentOrganizationDirectory {
    private ArrayList<Organization> governmentOrganizationList;

    public GovernmentOrganizationDirectory() {
        governmentOrganizationList = new ArrayList<>();
    }
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.GovernmentManage.getValue())){
            organization = new GovernmentManageOrganization();
            governmentOrganizationList.add(organization);
        }
        
        return organization;
    }
    
    
    
    public ArrayList<Organization> getGovernmentOrganizationList() {
        return governmentOrganizationList;
    }
    
    
    
}
