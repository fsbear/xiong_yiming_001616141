/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HouseManageOrganization;

import Business.GovernmentOrganization.GovernmentManageOrganization;
import Business.HospitalOrganization.Organization;
import Business.House.HouseDirectory;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class HouseManageOrganizationDirectory {
    private ArrayList<Organization> houseManageOrganizationList;
    private HouseDirectory houseDirectory;

    public HouseManageOrganizationDirectory() {
        this.houseManageOrganizationList = new ArrayList<>();
        this.houseDirectory = new HouseDirectory();
    }
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.HouseManagerWorker.getValue())){
            organization = new HouseManageWorkerOrganization();
            //houseDirectory = new HouseDirectory();
            houseManageOrganizationList.add(organization);
        }
        
        return organization;
    }

    public ArrayList<Organization> getHouseManageOrganizationList() {
        return houseManageOrganizationList;
    }

    public HouseDirectory getHouseDirectory() {
        return houseDirectory;
    }
    
    
}
