/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HouseManageOrganization;

import Business.HospitalOrganization.Organization;
import Business.Role2.HouseManageWorkerRole;
import Business.Role2.Role;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class HouseManageWorkerOrganization extends Organization{
    public HouseManageWorkerOrganization() {
        super(Organization.Type.HouseManagerWorker.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new HouseManageWorkerRole());
        return roles;
    }
}
