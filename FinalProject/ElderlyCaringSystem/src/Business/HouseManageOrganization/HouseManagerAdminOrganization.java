/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HouseManageOrganization;

import Business.HospitalOrganization.Organization;
import Business.Role2.HouseManageAdminRole;
import Business.Role2.Role;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class HouseManagerAdminOrganization extends Organization{
     public HouseManagerAdminOrganization() {
        super(Organization.Type.HouseManagerAdmin.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new HouseManageAdminRole());
        return roles;
    }
}
