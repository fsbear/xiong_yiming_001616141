/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Simulate;

import Business.Sensors.Sensor;

/**
 *
 * @author xiongyiming
 */
public class SimulationThread implements Runnable{
    
    private Sensor sensor;
    private double simulateresult;
    private double upperlimit;
    private double lowerlimit;
            

    public SimulationThread(Sensor sensor, double upperlimit,double lowerlimit) {
        this.sensor = sensor;
        this.lowerlimit = lowerlimit;
        this.upperlimit = upperlimit;
                 
    } 

    
    @Override
    public void run() {
        this.simulateresult = ((upperlimit-lowerlimit)* Math.random()+lowerlimit);
    } 

    public double getSimulateresult() {
        return simulateresult;
    }
    
    
}

