/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GuardianOrganization;

import Business.HospitalOrganization.Organization;
import Business.Role2.GuardianRole;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class GuardianOrganizationDirectory {
    private ArrayList<Organization> guardianOrganizationList;
    private ArrayList<GuardianRole> guardianList;

    public GuardianOrganizationDirectory() {
        this.guardianOrganizationList = new ArrayList<>();
        this.guardianList = new ArrayList<>();
    }
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.Guardian.getValue())){
            organization = new GuardianOrganization();
            //guardianList = new ArrayList<>();
            guardianOrganizationList.add(organization);
        }
        
        return organization;
    }

    public ArrayList<Organization> getGuardianOrganizationList() {
        return guardianOrganizationList;
    }

    public ArrayList<GuardianRole> getGuardianList() {
        return guardianList;
    }
    
}
