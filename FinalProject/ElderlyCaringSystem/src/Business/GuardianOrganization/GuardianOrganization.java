/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GuardianOrganization;

import Business.HospitalOrganization.Organization;
import Business.Role2.GuardianRole;
import Business.Role2.Role;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class GuardianOrganization extends Organization{
    
    
    public GuardianOrganization() {
        super(Organization.Type.Guardian.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new GuardianRole());
        return roles;
    }
    
    
    
}
