/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.VolunteerOrganization;

import Business.HospitalOrganization.Organization;
import Business.Role2.Role;
import Business.Role2.VolunteerCollegeRole;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class VolunteerCollegeOrganization extends Organization {
    public VolunteerCollegeOrganization() {
        super(Organization.Type.VolunteerCollege.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new VolunteerCollegeRole());
        return roles;
    }
}
