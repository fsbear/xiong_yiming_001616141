/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.VolunteerOrganization;

import Business.HospitalOrganization.HospitalDoctorOrganization;
import Business.HospitalOrganization.HospitalNurseOrganization;
import Business.HospitalOrganization.Organization;
import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class VolunteerOrganizationDirectory {
    private ArrayList<Organization> volunteerOrganizationList;

    public VolunteerOrganizationDirectory() {
        volunteerOrganizationList = new ArrayList<>();
    }

    
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.VolunteerCollege.getValue())){
            organization = new VolunteerCollegeOrganization();
            volunteerOrganizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.VolunteerCommunity.getValue())){
            organization = new VolunteerCommunityOrganization();
            volunteerOrganizationList.add(organization);
        }
        return organization;
    }

    public ArrayList<Organization> getVolunteerOrganizationList() {
        return volunteerOrganizationList;
    }
    
}
