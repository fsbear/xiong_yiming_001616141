/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class PersonDirectory {
    
    private ArrayList<Person> personList;

    public PersonDirectory(){
        personList = new ArrayList();
    }
    
    
    
    public ArrayList<Person> getPersonList() {
        
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
    
    public Person addPerson(){
        Person p = new Person();
        personList.add(p);
        return p;
        
        
    }
    public void deletePerson(Person p){
        personList.remove(p);
        
    }
    
    public Person searchPerson(String personName){
        for(Person a :personList){
            if(personName.equals(a.getName())){
                return a;
            }
        }
        return null;
    }
    
    public Person searchPersonID(int personid){
        for(Person a :personList){
            if(personid == a.getPersonID()){
                return a;
            }
        }
        return null;
    }
}
