/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.Date;

/**
 *
 * @author xiongyiming
 */
public class VitalSign {
    private float bloodpressure;
    private float weight;   
    public Date date;
    private int patientID;
    

    public VitalSign(){
        
        date = new Date();
    }

    public float getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(float bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }
    
    
    @Override
    public String toString() {
        return String.valueOf(date);
    }
}
