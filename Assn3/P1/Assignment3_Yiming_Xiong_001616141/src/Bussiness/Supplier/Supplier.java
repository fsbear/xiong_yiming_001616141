/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Supplier;

import Business.Supplier.Product.ProductCatalog;

/**
 *
 * @author xiongyiming
 */
public class Supplier {
    
    private ProductCatalog  productcatalog; 
    private String supplierName;
    
    public Supplier (String string) {
        productcatalog = new ProductCatalog() ;
        this.supplierName = string;

    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public ProductCatalog getProductcatalog() {
        return productcatalog;
    }

    public void setProductcatalog(ProductCatalog productcatalog) {
        this.productcatalog = productcatalog;
    }

}
