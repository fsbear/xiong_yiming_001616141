/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Supplier.Product;

/**
 *
 * @author xiongyiming
 */
public class Product {
    
    private String productName;
    private int price;
    private int avilNum;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String name) {
        this.productName = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAvilNum() {
        return avilNum;
    }

    public void setAvilNum(int avilNum) {
        this.avilNum = avilNum;
    }
    public Product(String string, int a, int b){
        this.productName = string;
        this.price = a;
        this.avilNum = b;
        
        
    }
    
    
}
