package UserInterface.StoreAdminRole;

import Business.Drug;
import Business.DrugCatalog;
import Business.DrugInventoryItem;
import Business.Store;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class AddInventoryJPanel extends javax.swing.JPanel {

    JPanel userProcessContainer;
    Store store;
    DrugCatalog dc;
    public AddInventoryJPanel(JPanel userProcessContainer,DrugCatalog dc,Store store){
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.store = store;
        this.dc = dc;
        
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Add Inventory");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Serial Num");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(101, 130, 100, 30));

        txtId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 130, 210, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Expiration Date");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(59, 180, 140, 30));

        txtPrice.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 180, 160, 30));

        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAdd.setText("Add Inventory");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 300, -1, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 300, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Drug Name:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, -1, 30));

        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 90, 210, -1));

        jLabel4.setText("Quantity");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 220, 110, 40));
        add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 230, 190, -1));
    }// </editor-fold>//GEN-END:initComponents
    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String name = txtName.getText();
        String serial = txtId.getText();
        String exp = txtPrice.getText();
        int q = Integer.parseInt(jTextField1.getText());
//        System.out.print(find());
        System.out.print(serial);

        if(dc.searchDrug(serial)!=null){
            if(store.getDrugInventory().searchDrugInventoryItem(serial)!=null){
            
                DrugInventoryItem drugInventoryItem = store.getDrugInventory().searchDrugInventoryItem(serial);
                if(drugInventoryItem.getDrug().getExpirationDate()==exp){
                    int newq = drugInventoryItem.getQuantity()+q;
                    drugInventoryItem.setQuantity(newq);
                
                
                    }
                }
                else{
                    Drug drug = new Drug();
                    drug.setDrugName(name);
                    drug.setSerialNumber(serial);
                    drug.setExpirationDate(exp);
                    DrugInventoryItem dii = store.getDrugInventory().addDrugInventoryItem(drug, q);
            
            
            
                }  
        
        }
        else{
            JOptionPane.showMessageDialog(null, "Please Create Drug first", "Warning", JOptionPane.WARNING_MESSAGE);
        }
}//GEN-LAST:event_btnAddActionPerformed
//private boolean find(){
//    String serial = txtId.getText();
//    for(Drug d : dc.getDrugCatalog()) {
//            if(d.getSerialNumber()== serial)
//            {  return  true;
//            
//            } 
//            else{
//                return false;
//            }
//    }return false;
//}
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ManageDrugInventoryJPanel manageProductCatalogJPanel = (ManageDrugInventoryJPanel) component;
        manageProductCatalogJPanel.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPrice;
    // End of variables declaration//GEN-END:variables

}


            

