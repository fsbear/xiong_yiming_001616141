/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author xiongyiming
 */
public class Store {
    
    private String storeLocation;
    private DrugInventory drugInventory;
    
    
    public Store() {
        drugInventory = new DrugInventory();
    }

    public String getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    public DrugInventory getDrugInventory() {
        return drugInventory;
    }
    @Override
    public String toString() {
        return storeLocation;
    }
}
