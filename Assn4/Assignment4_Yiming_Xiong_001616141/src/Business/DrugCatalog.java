/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class DrugCatalog {
    
     private ArrayList<Drug> drugCatalog;
     
     public DrugCatalog() {
        drugCatalog = new ArrayList<Drug>();
    }

    public ArrayList<Drug> getDrugCatalog() {
        return drugCatalog;
    }
     
     public Drug addDrug() {
        Drug d = new Drug();
        drugCatalog.add(d);
        return d;
    }
    
    public void removeDrug(Drug d) {
        drugCatalog.remove(d);
    }
    
    public Drug searchDrug(String serialNum) {
        
        for(Drug d : drugCatalog) {
            if(d.getSerialNumber().equals(serialNum))
            {
                return d;
            }
        }
        return null;
    }
    
}
