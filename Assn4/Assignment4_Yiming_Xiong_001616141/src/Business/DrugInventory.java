/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class DrugInventory {
    private ArrayList<DrugInventoryItem> drugInventoryItemList;
    
    public DrugInventory(){
        drugInventoryItemList = new ArrayList<DrugInventoryItem>();
    }

    public ArrayList<DrugInventoryItem> getDrugInventoryItemList() {
        return drugInventoryItemList;
    }

    public void setDrugInventoryItemList(ArrayList<DrugInventoryItem> drugInventoryItemList) {
        this.drugInventoryItemList = drugInventoryItemList;
    }
    public DrugInventoryItem addDrugInventoryItem(Drug d, int quantity){
        DrugInventoryItem di = new DrugInventoryItem();
        di.setDrug(d);
        di.setQuantity(quantity);
        
        drugInventoryItemList.add(di);
        return di;
    }
    public void removeDrugInventoryItem(DrugInventoryItem di){
        drugInventoryItemList.remove(di);
    }
    
    public DrugInventoryItem searchDrugInventoryItem(String serialNum) {
        
        for(DrugInventoryItem d : drugInventoryItemList) {
            if(d.getDrug().getSerialNumber() == serialNum)
            {
                return d;
            }
        }
        return null;
    }
    
}
