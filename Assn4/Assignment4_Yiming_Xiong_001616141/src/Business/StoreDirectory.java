/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class StoreDirectory {
    
    private ArrayList<Store> storeDirectory;
    
    public StoreDirectory() {
        storeDirectory = new ArrayList<Store>();
    }

    public ArrayList<Store> getStoreDirectory() {
        return storeDirectory;
    }
    public Store addStore() {
       Store s = new Store();
        storeDirectory.add(s);
        return s;
    }
    
    public void removeStore(Store s) {
        storeDirectory.remove(s);
    }
    
    public Store searchSupplier(String keyWord) {
        for(Store s : storeDirectory) {
            if(keyWord.equals(s.getStoreLocation())) {
                return s;
            }
        }
        return null;
    }
}
