/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author xiongyiming
 */


public class Pharma {
    private String pharmaName;
    private DrugCatalog drugCatalog;
    
    public Pharma(){
        drugCatalog = new DrugCatalog();
    }

    public String getPharmaName() {
        return pharmaName;
    }

    public void setPharmaName(String pharmaName) {
        this.pharmaName = pharmaName;
    }

    

    public DrugCatalog getDrugCatalog() {
        return drugCatalog;
    }

    public void setDrugCatalog(DrugCatalog drugCatalog) {
        this.drugCatalog = drugCatalog;
    }
@Override
    public String toString() {
        return pharmaName;
    }



}
