/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author xiongyiming
 */
public class PharmaDirectory {
    
    private ArrayList<Pharma> pharmaDirectory;
    
    public PharmaDirectory() {
        pharmaDirectory = new ArrayList<Pharma>();
    }

    public ArrayList<Pharma> getPharmaDirectory() {
        return pharmaDirectory;
    }

    
    
    public Pharma addPharma() {
       Pharma p = new Pharma();
        pharmaDirectory.add(p);
        return p;
    }
    
    public void removeSupplier(Pharma p) {
        pharmaDirectory.remove(p);
    }
    
    public Pharma searchSupplier(String keyWord) {
        for(Pharma p : pharmaDirectory) {
            if(keyWord.equals(p.getPharmaName())) {
                return p;
            }
        }
        return null;
    }
    
    
}
