/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author xiongyiming
 */
public class DrugInventoryItem {
    
    private Drug drug;
    private String exp;
    public static int threshold = 10;
    private String isshort;
    
    private int quantity;

    public Drug getDrug() {
        judge();
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }
    private String judge(){
        if(quantity<threshold){
            isshort = "Short";
        }
        else{isshort = "Enough";}
        return isshort;
    }

    public String getIsshort() {
        return isshort;
    }
    
    
}
