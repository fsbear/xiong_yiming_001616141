/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xiongyiming
 */
public class SalesPersonDirectory {
    private List<SalesPerson> salesPersonList;
    
    public SalesPersonDirectory(){
        salesPersonList = new ArrayList<SalesPerson>();
        
    }

    public List<SalesPerson> getSalesPersonList() {
        return salesPersonList;
    }
    public SalesPerson addSalesperson(){
        SalesPerson p = new SalesPerson();
        salesPersonList.add(p);
        return p;
    }
    
    
}
