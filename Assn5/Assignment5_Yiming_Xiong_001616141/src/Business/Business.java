package Business;


public class Business {

    private SupplierDirectory supplierDirectory;
    private MasterOrderCatalog masterOrderCatalog;
    private CustomerDirectory customerDirectory;
    private SalesPersonDirectory salesPersonDirectory;
    
    public Business() {
        supplierDirectory = new SupplierDirectory();
        masterOrderCatalog = new MasterOrderCatalog();
        customerDirectory = new CustomerDirectory();
        salesPersonDirectory = new SalesPersonDirectory();
                
        
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public SalesPersonDirectory getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(SalesPersonDirectory salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }
    
}
