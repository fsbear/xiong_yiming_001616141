/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;


public class Product {
    
    private String prodName;
    private int price;
    private double ceilingPrice;
    private double floorPrice;
    private int modelNumber;
    private int avail;
    private int saleVolume =0;
    private static int count =0;

    @Override
    public String toString() {
        return prodName; //To change body of generated methods, choose Tools | Templates.
    }

     public int getAvail() {
        return avail;
    }
    
    public void setAvail(int avail) {
        this.avail = avail;
    }
    
    public Product() {
    count++;
    modelNumber = count;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }
    
    public Product setVaryPrice(){
        this.ceilingPrice = this.price*1.3;
        this.floorPrice = this.price*0.7;
        return this;
        
        
    }

    public double getCeilingPrice() {
        return ceilingPrice;
    }

    public double getFloorPrice() {
        return floorPrice;
    }
    public int addSaleVolume(int q){
        this.saleVolume = this.saleVolume + q;
        return saleVolume;
        
        
    }

    public int getSaleVolume() {
        return saleVolume;
    }
    
    
}
