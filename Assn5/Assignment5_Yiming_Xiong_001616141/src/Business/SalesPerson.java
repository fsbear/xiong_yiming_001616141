/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author xiongyiming
 */
public class SalesPerson {
    private String salespersonName;
    private double commission;
    private int salesVolume;
    private int numBelowTarget = 0;
    private int numAboveTarget = 0;
    
    
    public SalesPerson(){
        commission = 0;
        salesVolume = 0;
    }

    public String getSalesPersonName() {
        return salespersonName;
    }

    public void setSalesPersonName(String salespersonName) {
        this.salespersonName = salespersonName;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public int getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(int salesVolume) {
        this.salesVolume = salesVolume;
    }
    public double addCommission(double a){
        this.commission = this.commission +a;
        return this.commission;
        
    }
    public int addSalesVolume(int a){
        this.salesVolume =  this.salesVolume +a;
        return  this.salesVolume;
        
    }

    public int getNumBelowTarget() {
        return numBelowTarget;
    }

    public void setNumBelowTarget(int numBelowTarget) {
        this.numBelowTarget = numBelowTarget;
    }

    public int getNumAboveTarget() {
        return numAboveTarget;
    }

    public void setNumAboveTarget(int numAboveTarget) {
        this.numAboveTarget = numAboveTarget;
    }
    
    @Override
    public String toString() {
        return salespersonName; 
    }
    
    
}
