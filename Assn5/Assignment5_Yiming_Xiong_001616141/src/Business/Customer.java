/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author xiongyiming
 */
public class Customer {
    private String customerName;
    private int totalSpend;
    private String phone;
    
    public Customer(){
        totalSpend = 0;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getTotalSpend() {
        return totalSpend;
    }

    public void setTotalSpend(int totalSpend) {
        this.totalSpend = totalSpend;
    }
    
    public int addTotalSpend(int a){
        this.totalSpend = this.totalSpend +a;
        return totalSpend;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
            
            
    @Override
    public String toString() {
        return customerName; 
    }
    
}
