/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xiongyiming
 */
public class CustomerDirectory {
    
    private List<Customer> customerList;

    public CustomerDirectory() {
    customerList = new ArrayList<Customer>();
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }
    
    public Customer addCustomer(){
        Customer p = new Customer();
        customerList.add(p);
        return p;
    }
    
}
