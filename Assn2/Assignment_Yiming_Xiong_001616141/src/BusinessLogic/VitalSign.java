/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.Date;

/**
 *
 * @author xiongyiming
 */
public class VitalSign {
    private float respiratory_rate;
    private float heart_rate;
    private float bloodpressure;
    private float weight;
   
    public Date date;
    

 
    
     

    public VitalSign(){
        
        date = new Date();
    }
//    public VitalSign(String f_name){
//        VitalSign vs = new VitalSign(f_name);
//        date = new Date();
//    }
    public float getRespiratory_rate() {
        return respiratory_rate;
    }

    public void setRespiratory_rate(float respiratory_rate) {
        this.respiratory_rate = respiratory_rate;
    }

    public float getHeart_rate() {
        return heart_rate;
    }

    public void setHeart_rate(float heart_rate) {
        this.heart_rate = heart_rate;
    }

    public float getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(float bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
    
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return String.valueOf(date);
    }
    
    
    
   }
    

    

    
    

//    @Override
//    public String toString() {
//        return date;
//    }

    
    
    
    
    

